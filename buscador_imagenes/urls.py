from django.conf.urls import patterns, include, url
from django.contrib import admin
from main.views import *


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'buscador_imagenes.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', HomePageView.as_view()),
)
